# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_12_15_122413) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "cfbs", force: :cascade do |t|
    t.string "team"
    t.string "time"
    t.string "opponent"
    t.string "rank"
    t.string "site"
    t.string "result"
    t.integer "attendance"
    t.integer "current_wins"
    t.integer "current_loses"
    t.integer "stadium_capacity"
    t.integer "fill_rate"
    t.boolean "new_coach"
    t.boolean "tailgaiting"
    t.integer "prcp"
    t.integer "snow"
    t.integer "snwd"
    t.integer "tmax"
    t.integer "tmin"
    t.string "opponent_rank"
    t.string "conference"
    t.integer "year"
    t.integer "month"
    t.integer "day"
    t.bigint "fbtvs_id"
    t.index ["fbtvs_id"], name: "index_cfbs_on_fbtvs_id"
  end

  create_table "fbtvs", force: :cascade do |t|
    t.string "site"
    t.string "tv"
  end

  add_foreign_key "cfbs", "fbtvs", column: "fbtvs_id"
end
