class CfbReferencetoFbtv < ActiveRecord::Migration[6.0]
  def change
    add_reference :cfbs, :fbtvs, foreign_key: true
  end
end
