class CreateCfb < ActiveRecord::Migration[6.0]
  def change
    create_table :cfbs do |t|
    t.string :team
    t.string :time
    t.string :opponent
    t.string :rank
    t.string :site
    t.string :result
    t.integer :attendance
    t.integer :current_wins
    t.integer :current_loses
    t.integer :stadium_capacity
    t.integer :fill_rate
    t.boolean :new_coach
    t.boolean :tailgaiting
    t.integer :prcp
    t.integer :snow
    t.integer :snwd
    t.integer :tmax
    t.integer :tmin
    t.string :opponent_rank
    t.string :conference
    t.integer :year
    t.integer :month
    t.integer :day

    end
  end
end
