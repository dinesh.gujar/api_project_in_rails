require 'test_helper'

class PagesControllerTest < ActionDispatch::IntegrationTest
  test "should get query1" do
    get pages_query1_url
    assert_response :success
  end

  test "should get query2" do
    get pages_query2_url
    assert_response :success
  end

  test "should get query3" do
    get pages_query3_url
    assert_response :success
  end

  test "should get query4" do
    get pages_query4_url
    assert_response :success
  end

end
