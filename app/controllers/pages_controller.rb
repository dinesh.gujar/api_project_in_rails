class PagesController < ApplicationController
  def query1
    @data1 = Cfb.group("year").order(:year).count
  end

  def query2
    @data2 = Fbtv.limit(10).group("tv").where("tv !='Not on TV'").order(count: :desc).count
  end

  def query3
    @data3 = Cfb.limit(10).group("site").order(count: :desc).count
    #@data3 = Fbtv.where("tv !='Not on TV'")
  end

  def query4
    @data4 = Cfb.group("year").where("Site='Aloha StadiumHonolulu, HI'").count
  end
end
