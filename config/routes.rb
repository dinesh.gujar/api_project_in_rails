Rails.application.routes.draw do
  get 'pages/query1'
  get 'pages/query2'
  get 'pages/query3'
  get 'pages/query4'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
